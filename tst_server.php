<?php
	error_reporting(E_ALL);

	/* Позволяет скрипту ожидать соединения бесконечно. */
	set_time_limit(0);

	/* Включает скрытое очищение вывода так что мы получаем данные
	 * как только они появляются. */
	ob_implicit_flush();

	declare(ticks = 1);
pcntl_signal(SIGTERM, "signal_handler");
	pcntl_signal(SIGINT, "signal_handler");

	$address = '127.0.0.1';
	$port = 10000;

	if (($sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) === false) 
	{
		echo "socket_create() failed: reason: " . socket_last_error() . "\r\n";
	}

	if (socket_bind($sock, $address, $port) === false) 
	{
		echo "socket_bind() failed: reason: " . socket_last_error($sock) . "\r\n";
	}

	if (socket_listen($sock, 5) === false) 
	{
		echo "socket_listen() failed: reason: " . socket_last_error($sock) . "\r\n";
	}

	do 
	{
		if (($msgsock = socket_accept($sock)) === false) 
		{
			echo "socket_accept() failed: reason: " . socket_last_error($sock) . "\r\n";
			break;
		}
		do 
		{
			if (false === ($buf = socket_read($msgsock, 2048, PHP_BINARY_READ))) 
			{
				echo "socket_read() failed: reason: " . socket_last_error($msgsock) . "\r\n";
				break;
			}
			echo $buf;
		} 
		while (true);
		socket_close($msgsock);
	} 
	while (true);

	socket_close($sock);

	function signal_handler($signal)
	{
		switch($signal)
		{
			case SIGTERM:
				print "Caught SIGTERM\r\n";
				//exit;
			case SIGKILL:
				print "Caught SIGKILL\r\n";
				//exit;
			case SIGINT:
				print "Caught SIGINT\r\n";
				//exit;
		}
	}
?>
